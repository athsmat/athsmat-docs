===============================================
 Documentation for Zenfit
==================================================

The main documentation for the site is organized into a couple sections:

* :ref:`user-docs`
* :ref:`feature-docs`
* :ref:`about-docs`

Information about development is also available:

* :ref:`dev-docs`
* :ref:`design-docs`


Contents:
------
.. _table-of-contents:

.. toctree::
   :maxdepth: 1

   index

.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   getting_started

.. _feature-docs:

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Feature Documentation
   
   zenfit_settings
   reporting
   d3js
   future_features

.. _dev-docs:

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Developer Documentation

   common_functions
   database_structure
   redux_framework
   used_wp_functions
   users

.. _design-docs:

.. toctree::
   :maxdepth: 2
   :caption: Designer Documentation

   design

.. _about-docs:

.. toctree::
   :maxdepth: 2
   :caption: About Zenfit

   about
