Users
=========

There is a special class of users called ``zenfit_user`` that will only have the capability of managing options in the Zenfit app.

User Roles and Capabilities
-----------------

Upon plugin activation, ``zenfit_user`` is created with the ability to 

+-----------------+------------+
| read            |  true      |
+-----------------+------------+
| manage_options  |  true      |
+-----------------+------------+
| manage_options  |  true      |
+-----------------+------------+

