About Zenfit
===========

A workout app designed for you.

.. include:: ../AUTHORS.rst

Version
-------

0.0.1

Date
-------

Development began August 18, 2015

To Update Documentation
----------------

http://sphinx-doc.org/contents.html

History
-----------

Some info about the history.

Change Logs
-----------

0.0.0 : First change in the log.