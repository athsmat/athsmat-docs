Redux Framework
====================

The Redux Framework is a framework for setting options in admin panels. The benefit is the ability to quickly set up options that are highly integrated with Wordpress features and other applications like the Google Fonts API. 

Documentation can be found here https://docs.reduxframework.com.

Never edit anything in the ReduxFramework folder. To add new menus and submenus edit the ``zenfit-redux-config.php`` file located under the admin directory.

The options are saved in the ``zen_options`` table currently in ``option_id`` 250 and 251 as serialized arrays that can easily accessed using the Redux API.

+----------------+--------------------------------+-----------------------------------------------------------------------------------------------+----------------------+
|  option_id     |   option_name                  |      option_value                                                                             |     autoload         |
+================+================================+===============================================================================================+======================+
|     250        | zenfit_options                 |   a:2:{s:12:"text-example";s:12:"Default Text";s:16:"textarea-example";s:12:"Default Text";}  |      yes             |
+----------------+--------------------------------+-----------------------------------------------------------------------------------------------+----------------------+
|     251        | zenfit_options_transients      |   a:2:{s:14:"changed_values";a:0:{}s:9:"last_save";i:1441167087;}                             |      yes             |
+----------------+--------------------------------+-----------------------------------------------------------------------------------------------+----------------------+