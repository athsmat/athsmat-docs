============
Installation
============

Two Options:

- Upload `.zip` file of zenfit plugin under plugins admin page. 
- FTP unzipped zenfit folder to plugins directory under wp-content directory.