Used Wordpress Functions
=====================

``add_action()`` : link to `action docs`_.

.. _action docs: https://codex.wordpress.org/Function_Reference/add_action

``add_filter()`` : link to `filter docs`_.

.. _filter docs: https://codex.wordpress.org/Function_Reference/add_filter