D3.js
========

How to work this javascript library with a few examples. Full documentation is here

Pie Charts
--------

- Be awesome
- Make things faster

Bar Graphs
------------

Explanation of how to implement bar graphs.

Histograms
----------

Explanation of how to implement histograms.

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: support@zenfitapp.org

License
-------

The project is licensed under the BSD license.
