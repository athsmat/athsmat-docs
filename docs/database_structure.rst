Database Structure
========

Structure of the database.

Users
-----------

Table: ``zendb_users``

+-----------+------------+-----------+---------------+-------------+----------+-----------------+---------------------+-------------+
| user_id   | user_login | user_pass | user_necename |  user_email | user_url | user registered | user_activation_key | user_status |
+===========+============+===========+===============+=============+==========+=================+=====================+=============+
|           |            |           |               |             |          |                 |                     |             |
+-----------+------------+-----------+---------------+-------------+----------+-----------------+---------------------+-------------+

Table: ``zendb_usermeta``

The usermeta id number goes up to 24 by default in Wordpress. Listed below will be 25 and up which is custom created user metadata.

+-----------+------------+-----------+---------------+
| umeta_id  |    user_id |  meta_key |   meta_value  |
+===========+============+===========+===============+
|     25    |    1       |   height  |   182.88 cm   |
+-----------+------------+-----------+---------------+

Table: ``zendb_workouts``

Example data is filled in for the first row to illustrate the data type.

+-----------+------------+---------------+-----------------+--------------+-----------+--------------+-------------------+
| user_id   |workout_date|  entry_date   |workout_category | workout_type |  num_sets | reps_per_set |length_of_exercise |
+===========+============+===============+=================+==============+===========+==============+===================+
|     1     | user input |  timestamp    | 0 = Arms (etc.) | benchpress   |   3       |     10       |  10 (min)         |
+-----------+------------+---------------+-----------------+--------------+-----------+--------------+-------------------+

Futher Explanation: 

 -`workout_date` : user inputted date of actual workout.

 -`entry_date` : date of user input for revision purposes and primary key.

 -`workout_category` : General category of the body such as arms, back, chest etc.

 -`workout_type` : More specific workout type like curls, benchpress, row, etc.

 -`num_sets` : How many total sets of specific workout.

 -`reps_per_set` : How many reps of each set. For people who did different reps per set they can just do one entry at a time and enter them as 1 set until we can think of a better way to do this.

 -`length_of_exercise` : Total time of exercise that we can use to calculate endurance or stamina.


Table: ``zendb_user_attributes``

Example data is filled in for the first row to illustrate the data type. All values will be stored as metric quantities for ease of calculation and an output function will show either imperial or metric based on the user settings

+-----------+---------------+-----------------+--------------+-----------+--------------+
| user_id   |  entry_date   | weight          | waistline    |  bicep    | quads        |
+===========+===============+=================+==============+===========+==============+
|     1     |  timestamp    | 150 kg          | 30      cm   |   30 cm   |     10 cm    |
+-----------+---------------+-----------------+--------------+-----------+--------------+