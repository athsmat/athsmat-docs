Reporting
========

Reporting on comparitive statistics of user workout data.

Strength
-------

Score on strength out of 100. Show Calculation for each.

Endurance
-------

Score on endurance out of 100.

Arms
-------

Score on arms out of 100.

Back
-------

Score on back out of 100.

Chest
-------

Score on chest out of 100.

Core
-------

Score on core out of 100.

Legs
-------

Score on legs out of 100.
